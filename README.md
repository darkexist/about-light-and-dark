# about-light-and-dark

about-light-and-dark is a Python library for darken/lighten color.

## This program can:

* **_Darken color_**

* **_Lighten color_**

## Usage

```
python -m brightness_changer 'd/l(darken/lighten) #RRGGBB(color) 0-100(brightness)'
```

## Examples

* [ ] python -m brightness_changer 'd #FF0080 50'

* [ ] python -m brightness_changer 'l #CD03BA 100'

## Formula

* **for darken**

    *color_value * (1 - percent / 100)*

* **for lighten**

    *color_value + 255 * percent / 100*

## Inspiration

Idea from [here](https://webformyself.com/sass-funkcii-usilenie-stilej-i-rasshirenie-vozmozhnostej/)

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)

### Thank you for reading!

![alt sci](https://saudiphilosophy.com/wp-content/uploads/2021/10/shutterstock_1072059917-scaled.jpg)
