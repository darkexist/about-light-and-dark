"""
Проверка входных данных.
"""


def raw_cb(raw_str):
    """
    Проверка входных данных.

    Разбивает входное значение на три переменные и преобразует аргумент
    яркости в целое число и, в случае успеха, проверяет удовлетворяет
    ли аргумент цвета необходимым условиям(длина 6 или 7 символов, содержание
    только допустимых символов), в случае успеха возвращает проверенные
    значения.

    Args:
        raw_str: Строка с введёнными пользователем данными.

    Returns:
        Введённые пользователем данные.

    Raises:
        ValueError, AttributeError.

    Examples:
        >>> raw_cb('l #FF0080 50')
            '('l', 'ff0080', 50)'
        >>> raw_cb('l #FF0080 50 99')
            None
        >>> raw_cb('l #FF0080 500')
            None
    """
    accept_alph = '0123456789abcdef'
    try:
        mode, color, bright = raw_str.split()
        bright = int(bright)
    except ValueError:
        print('Параметр яркости должен быть целым числом')
    except AttributeError:
        print('Минимум 3 аргумента')
    else:
        if len(color) == 7 and color[0] == '#':
            color = color[1:]
        color = color.lower()
        if all(i in accept_alph for i in color) and len(color) == 6 and \
                0 <= bright <= 100:
            return mode, color, bright
        else:
            print('Введены некорректные данные')
